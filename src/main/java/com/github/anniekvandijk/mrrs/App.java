package com.github.anniekvandijk.mrrs;

import com.github.anniekvandijk.mrrs.domain.Facility;
import com.github.anniekvandijk.mrrs.domain.MeetingRoom;
import com.github.anniekvandijk.mrrs.repository.RoomRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;
import static spark.Spark.*;

public class App {

    private static RoomRepository roomRepository;

    public static void main(String[] args) {

        roomRepository = new RoomRepository();

        List<Facility> facilities = new ArrayList<>();
        facilities.add(new Facility("Beamer"));
        facilities.add(new Facility("Telephone"));

        MeetingRoom room1 = new MeetingRoom("Room 1", "Location 1", 10);
        MeetingRoom room2 = new MeetingRoom("Room 2", "Location 2", 4, facilities);
        roomRepository.add(room1);
        roomRepository.add(room2);
        port(8080);

        get("/meetingroom", (req, res) -> {
            res.type("application/json");
            return getAll();
        });
        get("/meetingroom/:name", (req, res) -> {
            res.type("application/json");
            return getFirstByName(req.params(":name"));
        });
    }

    private static String getAll() {

        List<MeetingRoom> rooms = roomRepository.getAll();
        return new Gson().toJson(rooms);
     }

    private static String getFirstByName(String name) {

        List<MeetingRoom> rooms = roomRepository.getByName(name);
        if (rooms.size() == 0 ) {
            return noRecordsFound();
        }
        return new Gson().toJson(rooms.get(0));
    }

    private static String noRecordsFound() {
       return new Gson().toJson("No records found");
    }
}
